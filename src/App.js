import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home'
import Product from './pages/Product';
import Login from './pages/LoginGoogle';
function App() {
  return (
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path='/*' element={<Home/>}/>
      <Route path='/products' element={<Product/>}/>
      <Route path='/login' element={<Login/>}/>
    </Routes>
  );
}

export default App;
