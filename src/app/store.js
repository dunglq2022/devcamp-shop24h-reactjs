import { combineReducers, createStore } from "redux";
import contentEvent from '../component/Reducer/contentEvent';
const appReducer = combineReducers({
    contentReducer: contentEvent
});

const store = createStore(
    appReducer,
);

export default store;