import Slider from "react-slick";
import image2 from '../../image/slider2-skybook1-1920x599.jpg'
import "../../../node_modules/slick-carousel/slick/slick"; 
import "../../../node_modules/slick-carousel/slick/slick-theme.css";

function Carousel () {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };
    return(
        <>
            <div>
                <Slider {...settings}>
                    <div>
                        <img src={image2} width={'auto'} alt="Slides"/>
                    </div>
                            
                </Slider>
            </div>
        </>
    )
}
export default Carousel;