
import Header from "../Header/Header";
import Carousel from "./Carousel";
import LasterPost from "./LasterPost";

function Content () {
    return(
        <>
        <Carousel/>
        <LasterPost/>
        </>
    )
}
export default Content;