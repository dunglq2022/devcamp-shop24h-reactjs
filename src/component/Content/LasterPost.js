import { Grid,  Box, Container } from "@mui/material";
import { Typography, CardContent, Card, Button, AspectRatio } from '@mui/joy';
import axios from 'axios';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import {Paper  } from "@mui/material";
import { styled } from '@mui/material/styles';

function LasterPost () {
    const dispatch = useDispatch();
    const {arrProducts} = useSelector((reduxData) => reduxData.contentReducer);
    const [limit, setLimit] = useState(9)

    useEffect(() => {
        let config = {
          method: 'get',
          maxBodyLength: Infinity,
          url: `http://localhost:8000/api/products?limit=` + limit
        };
    
        axios
          .request(config)
          .then((response) => {
            dispatch({
              type:'GET_PRODUCTS',
              payload: response.data.products
            });
          })
          .catch((error) => {
            console.log(error); 
          });
          }, [limit, dispatch]);
      
      const handleClickViewAll = () => {
            setLimit(0)
        }

      const Item = styled(Paper)(({ theme }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      }));
  
    return(
      <Container>
            <Box>
                <Grid container spacing={2}>
                    <Grid xs={3} item>
                        <Item>Sidebar</Item>
                    </Grid>
                    <Grid xs={9} item>
                      <Grid container>
                        {arrProducts.map((item, index) => {
                          let date = new Date(item.createdAt)
                          let day = date.getDate();
                          let month = date.getMonth();
                          let year = date.getFullYear();
                          const numberWithCommas = (number) => {
                            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                          };
                          const formattedPrice = numberWithCommas(item.buyPrice);
                          return(
                            <Grid
                            sx={{pr:3, pl:3, pt:1, pb:1}}
                            item
                            xs={12} sm={6} md={4}
                            key={index}
                            >
                              <Card sx={{ width: '100%' }}>
                                <div>
                                  <Typography level="title-md"
                                  sx={{
                                    whiteSpace: 'nowrap',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    maxWidth: '100%'
                                  }}>{item.name}</Typography>
                                  <Typography level="body-sm">{`ngày ${day} tháng ${month} năm ${year}`}</Typography>
                                </div>
                                <AspectRatio minHeight="250px" maxHeight="350px">
                                  <img
                                    src={item.imageUrl}
                                    loading="lazy"
                                    alt=""
                                  />
                                </AspectRatio>
                                <CardContent orientation="horizontal">
                                  <div>
                                    <Typography level="body-xs">Giá sách:</Typography>
                                    <Typography fontSize="lg" fontWeight="lg">
                                      {`${formattedPrice} VNĐ`}
                                    </Typography>
                                  </div>
                                  <Button
                                    variant="solid"
                                    size="md"
                                    color="primary"
                                    aria-label="Explore Bahamas Islands"
                                    sx={{ ml: 'auto', alignSelf: 'center', fontWeight: 600 }}
                                  >
                                    Buy
                                  </Button>
                                </CardContent>
                              </Card>
                            </Grid>
                          )
                        })}
                        <Grid sx={{mt:2}} container justifyContent={'center'}>
                          <Button onClick={handleClickViewAll}>
                            View All
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    )
}
export default LasterPost;