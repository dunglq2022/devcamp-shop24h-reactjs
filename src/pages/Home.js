import Content from '../component/Content/Content';
import Header from '../component/Header/Header';

function Home() {
  return (
    <>
      <Header/>
      <Content/>
    </>
  );
}

export default Home;