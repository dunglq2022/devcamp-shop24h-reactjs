import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import auth from "../firebase";
import { Grid, Button, Typography, Divider, TextField, Box} from "@mui/material";
import { green, grey, red } from "@mui/material/colors";
import GoogleIcon from '@mui/icons-material/Google';
import { useState } from "react";
import { useHref } from "react-router-dom";

const provider = new GoogleAuthProvider();

function Login() {
    const [user, setUser] = useState(null);
    const href = useHref;
    const redirectURL = href ('/');

    const loginGoogle = () => {
        signInWithPopup(auth, provider)
        .then((result) => {
            setUser(result.user)
            console.log(result.user);
            
            //Chuyển hướng trang về trang chủ
            console.log(redirectURL)
            window.location.href = redirectURL;          
        })
        .catch((error) => {
            console.log('Error', error)
        })
    };
    
    return (
        <Box sx={{
            display: 'flex',  
            flexDirection:  'column',
            justifyContent:'center' , 
            alignItems:"center",
            backgroundColor: grey[300],
            height: '1000px'
        }}>
            <Grid sx={{
                display: 'flex',
                justifyContent:'center',
                mt: 5,
            }}>
                <Grid item container sx={{
                    width: '500px',
                    backgroundColor: grey[50]
                    }}
                    xs={12}
                    >
                    <Grid xs={12} item sx={{
                        display: 'flex',
                        justifyContent:"center",
                    }}>
                        <Button sx={{
                            backgroundColor: red[500],
                            color: grey[50],
                            width: '350px',
                            borderRadius: '50px',
                            height:'50px',
                            fontSize: '16px',
                            '&:hover': {
                                backgroundColor: red[800]
                            },
                            mt: 10
                        }}
                        onClick={loginGoogle}
                        startIcon={<GoogleIcon/>}
                        >
                            <Typography>{'Sigin with '}<span style={{fontWeight: '700'}}>Google</span></Typography>
                        </Button>
                    </Grid>
                    <Grid xs={12} item sx={{
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <Divider sx={{
                            width: '350px',
                            borderColor: grey[700],
                            mt: 8,
                        }}/>
                    </Grid>
                    <div style={{
                        height: '45px',
                        width: '45px',
                        border: `1px solid ${grey[400]}`,
                        borderRadius: '50%',
                        position: 'relative',
                        left: 235,
                        bottom: 25,
                        backgroundColor: 'white',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'               
                        }}>
                            <Typography variant="caption" sx={{ fontWeight: 'bold', fontSize: '16px' }}>
                                {'OR'}
                            </Typography>
                        </div>
                    <Grid xs={12} item sx={{
                        mt: 2,
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        <TextField
                        label={'User Name'}
                        type={'text'}
                        sx={{
                            width: '350px',
                        }}
                        />
                    </Grid>
                    <Grid xs={12} item sx={{
                        mt: 2,
                        mb: 2,
                        display: 'flex',
                        justifyContent: 'center',
                    }}>
                        <TextField
                        label={'Password'}
                        type={'password'}
                        sx={{
                            width: '350px',
                        }}
                        />
                    </Grid>
                    <Grid xs={12} item sx={{
                        display: 'flex',
                        justifyContent:"center",
                    }}>
                        <Button sx={{
                            backgroundColor: green[500],
                            color: grey[50],
                            width: '350px',
                            borderRadius: '50px',
                            height:'50px',
                            fontSize: '16px',
                            '&:hover': {
                                backgroundColor: green[800]
                            },
                            mb: 5
                        }}
                        >
                            <Typography>{'Sign in '}</Typography>
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid xs={12} sx={{
                mt: 2
            }}>
                    <Typography>{`Don't have an account? `}<span style={{
                        color : green[500]
                    }}>Sign up here</span></Typography>
            </Grid>
        </Box>
    )
}
export default Login;

